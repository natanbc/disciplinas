# SSC0513 Organizacao e Arquitetura de Computadores

# Curso BSI - BSI - BSI - BSI - BSI - BSI - BSI - BSI


- Professor: Eduardo do Valle Simões
- Email: simoes@@@@@@@icmc.usp.br
- Departamento de Sistemas de Computação – ICMC - USP
- Grupo de Sistemas Embarcados e Evolutivos
- Laboratório de Computação Reconfigurável

## Alunos de 2022 - Segundo semestre 

- 1ª Prova: Dia 06/12/2022 

- REC:15/03/2023  -> 14h  -> Na minha sala (3134)

- Lista de Presença - Favor assinar a Lista de Presença durante o horario das aulas
- Essa disciplina irá utilizar o projeto do Processador ICMC, que é um processador RISC de 16 bits implementado em FPGA - todas as ferramentas de software e hardware estão disponíveis no Github do projeto: https://github.com/simoesusp/Processador-ICMC
- Os alunos serão divididos em grupos de 3-4 alunos para implementação do trabalho prático para a avaliação 
- TODOS OS ALUNOS PRECISAM APRESENTAR O TRABALHO PESSOALMENTE!!


## Apresentação dos Trabalhos: Insira as informações do seu grupo no arquivo do GoogleDocs:

- https://docs.google.com/spreadsheets/d/1cEHrikbdrBwx4rU7ft6EMRMra5d1h3LnTkKu7U8wXXk/edit?usp=sharing 


## Processador ICMC - https://github.com/simoesusp/Processador-ICMC

- Nossa disciplina irá usar um Processador desenvolvido pelos próprios alunos do ICMC disponível neste repositório do github

## Simulador para programação em Assembly 
- Nossa disciplina irá usar um simulador para desenvolver programas em linguagem Assembly que poderá ser encontrado para Windows, Linux e MacOS em: https://github.com/simoesusp/Processador-ICMC/blob/master/Install_Packages/
- Para Windows, fiz um link super fácil de instalar: https://github.com/simoesusp/Processador-ICMC/blob/master/Install_Packages/Simulador_Windows_Tudo_Pronto_F%C3%A1cil%20(1).zip
  - Esse zip já vem inclusive com o sublime configurado para escrever o software (incluindo a sintaxe highlight) e o montador e o simulador já configurado para ser chamado com a tecla F7
  - Para instalar basta fazer o download na area de trabalho ou na pasta Documentos
  - Entrar na pasta ..\Simulador\Sublime Text 3
  - Executar o sublime: "sublime_text.exe"
  - Se ele pedir, NÃ0 FAÇA O UPDATE !!!!!!!!!!!!!!!
  - Vá em File - Open File e volte uma pasta para ..\Simulador\
  - Abra o sw em Assembly chamado Hello4.ASM
  - Teste se está tudo funcionando chamando o MONTADOR e o SIMULADOR com a tecla F7
  - Apartir daí pode-se salvar o sw com outro nome e fazer novos programas
  - Apenas preste atenção para estar na pasta ..\Simulador\
  - Se der o erro: [Decode error - output not utf-8] é porque você não está na pasta ..\Simulador\
  - ... Ou basta mudar o formato para utf-8 e salvar...


## Avaliação
- A avaliação será por meio de uma PROVA (50% da nota) sobre o conteúdo teórico e da apresentação de um TRABALHO (50% da nota) - Implementação de um Jogo em Linguagem Montadora (Assembly) no simulador (links acima)
  - Não pode tirar menos de CINCO nas avaliações
- Os alunos serão divididos em grupos de 3-4 alunos para implementação do trabalho prático para a avaliação 
- TODOS OS ALUNOS PRECISAM APRESENTAR O TRABALHO PESSOALMENTE!!

### Apresentação dos trabalhos Turma 2022 - Segundo semestre:
- Apresentação dos trabalhos será em reunião PRESENCIAL com o Professor nas últimas semanas de aula: marcar o dia e horário escolhidos para apresentacao na tabela do arquivo:  ==> https://docs.google.com/spreadsheets/d/1cEHrikbdrBwx4rU7ft6EMRMra5d1h3LnTkKu7U8wXXk/edit?usp=sharing

  - Apresentação dos trabalhos será presencial na Sala de aula nas últimas semanas do Semestre

  - Insira seu projeto e reserve um horario para apresentar no arquivo do GoogleDocs, informando: TÍTULO DO PROJETO - nomes dos alunos - NUMERO USP dos alunos - link pro github/gitlab do seu Trabalho

## Trabalho da Disciplina:
- 1) Implementar um Jogo em Assembly (tema Livre)
- 2) Apresentar o Jogo rodando no simulador do Processador para o Professor

### Documentação do Trabalho no Github/Gitlab
- Criar uma conta sua no Github ou no Gitlab
- Os projetos devem conter um Readme explicando o projeto e o software deve estar muito bem comentado!!
- Incluir no seu Github/Gitlab: o JOGO.ASM e caso tenha alterado, o CHARMAP.MIF. 
- Obrigatório: incluir um VÍDEO DE VOCE explicando o projeto (pode ser somente uma captura de tela...) - Upa o vídeo no youtube ou no drive e poe o link no Readme. ==> Não coloque o Vídeo no Github/Gitlab
- Além do VÍDEO DE VOCË explicando o projeto, TODOS OS ALUNOS PRECISAM APRESENTAR O TRABALHO PESSOALMENTE na Sala de aula

## Segundo Trabalçho para Substituir a Prova

- Dicas de como acrescentar uma nova instrução no MONTADOR - https://gitlab.com/simoesusp/disciplinas/-/tree/master/SSC0119-Pratica-em-Organizacao-de-Computadores/Modificar_Montador


# Material Didático - 2022 - Segundo Semestre

## Processador ICMC
- Link para o projeto do nosso próprio Processador: https://github.com/simoesusp/Processador-ICMC

## Simulador em C
- Link para o código fonte do Simple Simulator: https://github.com/simoesusp/Processador-ICMC/tree/master/Simple_Simulator

## Desenho da Arquitetura do nosso Processador ICMC
- Link para visualisar o Desenho na Ferramenta FIGMA: https://www.figma.com/file/mHQmhfvLiwmIRXkHlFDTyn/CPU_ICMC-team-library?node-id=415%3A589

## Arquitetura do nosso Processador ICMC:  
- https://github.com/simoesusp/Processador-ICMC/blob/master/Manual/ProcessadorICMC2.pdf

## Gerador de Caracteres e Telas do Gustavo: 
- https://github.com/GustavoSelhorstMarconi/Create-Screens-in-Assembly-with-python


# Planejamento das Aulas 2022 Segundo Semestre
- Note que cada aula do roteiro a seguir pode levar na prática várias aulas...

### Aula 1
- Apresentacao da Disciplina

### Aula 2
- Revisão e introdução geral ao conteudo de toda a Disciplina.
  - Transistores -> Portas Lógicas -> Registradores, Memória, ULA -> Maquina de controle -> Arquitetura de um Processador -> RISC vs CISC -> Pipeline -> Paralelismo -> MIMD (Multiple Instruction Stream, Multiple Dada Stream)

## Aula 3
### Roteiro do conteúdo a ser coberto nas proximas aulas:
- 1.-	Overclock
  - 1.1-	Aquecimento, refrigeração
  - 1.2-	Operação de uma CPU
  - 1.3-	Lei de Ohm
  - 1.4-	O transistor como chave - MOSFET
  - 1.5-	Construção de portas lógicas com transistores
  - 1.6-	Atrasos lógicos na propagação de sinais
  - 1.7-	Capacitores
  - 1.8-	Fan-out

- Material: 
  - Video PC mergulhado em óleo: https://drive.google.com/file/d/1LbdmO4tx4ec-LVSuJhp5HWZA4U3kO_uz/view?usp=sharing

  - Konrad Zuse - http://www.geocities.ws/hifi_eventos/Z1.html

  - Arquitetura do nosso Processador ICMC - https://gitlab.com/simoesusp/disciplinas/-/blob/master/SSC0180-Eletronica-para-Computacao/SSC0511-Organizacao-de-Computadores-Digitais_instrucoes_10_2016.pdf

  - Maq Controle Babage Arduino - iTnkercad: https://www.tinkercad.com/things/4EVIesUUlub
  - Linguagem de Programação do Babage - https://docs.google.com/document/d/1o4VXz_-rqT4DdeEFDnQiyRqTgo9NAKdWRpq_G3zO0YQ/edit?usp=sharing


  - Porta NOT (nMOS e CMOS) http://tinyurl.com/uk6az46

  - Liga LED e tensão de ativação do transistor - https://tinyurl.com/yhbob6lz

  - Porta AND (nMOS) http://tinyurl.com/ukexy4p

  - Porta OR (nMOS) http://tinyurl.com/tkajd8r

  - Porta AND (CMOS) http://tinyurl.com/wd6xpb7

  - Porta OR (CMOS) http://tinyurl.com/rp6qbq5

  - Capacitor - https://tinyurl.com/y6tyo82m

  - How to cook hotdogs with a 20000 volt capacitor bank - https://www.youtube.com/watch?v=DQ67njnNaxw 

  - Cadeia NOT FANOUT(nMOS) http://tinyurl.com/sjp8cq8

  - Cadeia NOT FANOUT(CMOS) http://tinyurl.com/vke6tj7

  - Cadeia NOT FANOUT 5V-10V (CMOS) http://tinyurl.com/ua8m7xh

## Aula 4 --> Programação em linguagem Montadora (Assembly)

- Compilando um Programa de C -> Assembly -> Binary - https://gitlab.com/simoesusp/disciplinas/-/blob/master/SSC0511-Organizacao-de-Computadores-Digitais/MaterialAulaDistancia/Compilando_um_Programa.c

- MONTADOR - https://github.com/simoesusp/Processador-ICMC/tree/master/NovoMontadorLinux

- SIMULADOR - https://github.com/simoesusp/Processador-ICMC/tree/master/Install_Packages

- Link para o projeto do nosso próprio Processador: https://github.com/simoesusp/Processador-ICMC

- RPG bem legal -> Battle of Tiers: Jonas Wendel Costa de Souza - https://github.com/4Vertrigo/BattleOfTiers


## Aula 5 --> Implementacao do nosso Processador
### Roteiro do conteúdo a ser coberto nas proximas aulas:
- 1.-	Apresentacao das Ferramentas:
  - 1.1-	Simulador Normal
  - 1.2-  Simple Simulator
  - 1.3-  Testa CPU
  - 1.4-  Desenho na ferramenta FIGMA
- 2. Projeto das Instrucoes do Processador
  - 2.1-  NOP
  - 2.2-  LOADN
  - 2.3-  LOAD
  - 2.4-  STORE
  ...

- Material: 
  - Link para a Aula Gravada (08/10/21) - https://drive.google.com/file/d/1b259fT0bFdzSXMUy-jr_eYg1NE_kgxpX/view?usp=sharing
  - Link para visualisar o Desenho na Ferramenta FIGMA: https://www.figma.com/file/mHQmhfvLiwmIRXkHlFDTyn/CPU_ICMC-team-library?node-id=415%3A589

  - Implementacao do nosso Processador: Loadi, Storei e Move
- Link para a Aula Gravada (19/10/21) - https://drive.google.com/file/d/1ORU9T3jdLUY6PiTUyIHH7a-jEqGmioN2/view?usp=sharing

## Aula 6 --> Memória e Cache

## Aula 7 --> Periféricos e Barramentos
- Transparencias sobre Barramentos: https://gitlab.com/simoesusp/disciplinas/-/blob/master/SSC0511-Organizacao-de-Computadores-Digitais/MaterialAulaDistancia/A02_Barramento_short2.odp
- Video explicando Barramentos (assistir a partir de 1h:15m:00s ) - https://drive.google.com/file/d/1IJDyGPvWEbK4dU3slQh02yU3WE4WNi7D/view?usp=sharing

## Aula 8 --> RISC vs CISC

## Aula 9 --> Pipeline 

## Aula 10 --> Paralelismo 

## MIMD (Multiple Instruction Stream, Multiple Dada Stream)




## Lista de projetos 2021

### Grupo  - Titulo - Link	

- 2	    - Tetris ICMC   - https://github.com/silmarp/Tetris-ICMC-Assembly		
- 3	    - Ludo ICMC     - https://github.com/GustavoOM/Ludo-ICMC		
- 7	    - Passos Word   - https://github.com/Augustofa/PassosWord		
- 8	    - The Riddler	  - https://github.com/Ryrden/SSC0511-The-Riddler		
- 9	    - 21	          - https://github.com/anleefc/21		
- 10    - ICMC Space	  - https://github.com/joao3/ICMC-Space		
- 11    - Shisima       - https://github.com/mrlFranchi/Shisima-Org-Comp		
- 12    - Blackout ICMC	- https://github.com/mordonha/ORGCOMP.2022.GAME		
- 13    - Evil Square	  - https://github.com/joelcoelho29/evil-square	
- 28    - My Hash	      - https://github.com/UesleiPina/My-Hash	


## Lista de projetos 2020
- GRUPO 6 - Adrian Pereira da Silva, João Vitor Diógenes, Marcio Hideo Ishikawa, Julio Igor Casemiro Oliveira - https://gitlab.com/marcioHideo

- GRUPO 3 - Iara Duarte Mainates, Lucas Caetano Procópio e Geraldo Murilo Carrijo Viana Alves da Silva
https://gitlab.com/trabalho-scc0511

- GRUPO 7 - Ellian Carlos, Giovanna Fardini, Thales Damasceno e Vinicius Baca - https://github.com/gifardini/SSC0511-2020 

- GRUPO 2 - Matheus Odebrecht Oliveira, Lucas Carneiro de Souza, Uriel Ramiro Munerato e Victor Kenji - https://gitlab.com/lucasc014/trabalho-1-org-comp 

- GRUPO 10 - Jonattan Willian da Silva - https://github.com/Th3Jow/Arrow-Hero 

- GRUPO 11 -João Paulo Garcia (https://github.com/joaousp), Guilherme Cremasco Gulmini (https://github.com/GuiCremasco) Amanda Lindoso Figueiredo ( https://github.com/alfunny ), https://github.com/joaousp/AssemblyICMCsnake - jogo em assembly

- GRUPO 4 - Bruno Alves Venancio, Jade Bortot de Paiva , Pedro José Garcia, Thiago Rodrigues da Cunha Marafeli
https://github.com/pedrogarcia8/snake Jogo Snake, https://github.com/pedrogarcia8/funcaoProcessador Função para o processador

- GRUPO 5 - Samuel Rubens Souza Oliveira , João Pedro Alonso Almeida, Gabriel Fontes, Pedro Silva Lima - https://github.com/rubenszinho/OrgComp-Projetos


- GRUPO 8 - Ricardo Atakiama(https://gitlab.com/Naneshoru), Ana Carolina Simões Ramos, Rafael Belisario de Oliveira, Tayane Leandro Guerrero.

- Grupo 12 - Altair Fernando Pereira Junior (https://gitlab.com/httpjnr)

- GRUPO 11 -João Paulo Garcia (https://github.com/joaousp), Guilherme Cremasco Gulmini (https://github.com/GuiCremasco) Amanda Lindoso Figueiredo ( https://github.com/alfunny ), https://github.com/joaousp/AssemblyICMCsnake - jogo em assembly

- GRUPO 9 - Lucas Silva Neves -ETQuest  (https://gitlab.com/LucasNeves159/ORGCOMP020/-/tree/master/Assembly%20game), Aríthissa Vitória Silva Carvalho (https://gitlab.com/arithissa/SSC0511-Trabalho1.git), João Pedro de Andrade (https://gitlab.com/joaosccpedro)



## Lista de projetos 2019

- Arrows (Bruno Pagno, João mello, Lucas Saliba): https://github.com/bruno-pagno/JogoSetas
- Snake 3 fases aumentando nível (Henrique Francischeti Calil) https://gitlab.com/henriquefcalil/snake---org-comp/blob/master/snake.asm
- Key Bomb Membros : Gabriel Guimarães Vilas Boas Marin, Igor Guilherme Pereira Loredo, Felipe Sampaio Amorim, Link : https://github.com/Mapromu/Key-Bomb
- Verme do Tusca (Raphael Melloni Trombini, https://gitlab.com/raphaeltrombini/verme-do-tusca/tree/master
- Game_of_Life_autômato celular (William Carlos Giovanetti Gomes Link: https://github.com/biw7/Game_of_life-Assembly
- RPG Universitário (Lucas Braga, Sen Chai, Vinicius Carvalho) https://github.com/vgdcarvalho/lab_org/tree/master
- Quiz (Bruno Maximo, Álefe Silva, Pedro Chrisfofoletti) https://github.com/brunomaximom/quiz
- Quase golfe (Guilherme Filipe Feitosa dos Santos e Leila Gomes Ferreira https://github.com/leilagomesf/jogo_assembly
- ICMC WARS: Gabriel Cocenza, Ingrid Otani, Maria Vitória Inocencio - https://github.com/gabrielcocenza/ICMC_WARS
- Number Sequence: Marcio Guilherme Vieira Silva, Maurício José da Silva Filho, Rafael Garcia Fortunato - https://github.com/mjsilvafilho/org_comp
- Nova Velha: Daniel De Marco Fucci - https://github.com/danikyl/Velha_Nova/blob/master/velha.asm
- FlappyBird: Joao Vitor Salvini, Felipe Vinicius de Souza, Sara Dib, Joao Pedro Kosur - https://gitlab.com/jv_salvini/flappybird/blob/master/Flappy.asm



### Lista de projetos 2018
- BlackJack: Andrey Garcia, Gabriel Rodrigues Machado, Markel Berestov, Rafaela Souza Silva - https://github.com/GabrielMachado11/BlackJackAssembly
- Arrow: Gabriel Muniz Morao, Juliana Yendo, Stella Granatto Justo - Repositorio: https://github.com/stellajusto/arrow-assembly | Youtube: https://www.youtube.com/watch?v=qqL6hVRvEkc
- Ultra Alien Fighter: Juliana de Mello Crivelli - https://github.com/jumc/ultra-alien-fighter
- BreakOut: Joao Marco Barros, Leonardo Toledo- Repositorio: https://github.com/JoaoMOBarros/BreakOut
- Snake: Julio Ng, Caio Ostan - https://github.com/juliongusp/asm
- Snake Bomb: Raphael Medeiros, Matheus Sanchez, Sergio Piza - https://github.com/rmedeiros23/Snake-Bomb
- DinoFuga: Victor Momenté, Vinicius Cortizo, João Victor Alves - https://github.com/VictorMomente/AssemblyGame
- Em Buscar do corote perdido: Marco Antonio Gallo, Murilo Kazumi de Freitas, Rubens Galdino - https://github.com/rubensgaldinojr/assemblygame-corote
- Snake: Orlando Pasqual Filho - https://github.com/NirvanaZen/SSC0511-Organizacao-de-Computadores-Digitais
- Snake: Guilherme Queiroz/ Pedro Fazio - https://github.com/Guilherme292/Projeto-Snake-ORG-Comp-I
- DPJump: Leandro Giusti Mugnaini/Matheus Borges Kamla/Bruno Ribeiro Helou - https://github.com/leandromugnaini/jogoassembly
- Velha: Caio Alarcon, Denis Alexandre, Bruno Menzzano https://github.com/CaioAlarcon/projetos/tree/master/velha
- Snake Trifásico: Eduardo Zaboto Mirolli, Leonardo Bellini dos Reis, Vinícius Torres Dutra Maia da Costa - https://github.com/LeoBellini/jogo
- Donkey Kong: Leonardo Moreira Kobe, Matheus Bernardes dos Santos - https://github.com/lmkobe/DonkeyKongGame
- CampoMinado: Beatriz Guidi, Rafael Doering Soares: https://bitbucket.org/bguidi/jogoorgcompbeatrizrafael/src
- Invasão espacial: Gabriel Dos Santos Brito, Luiz Miguel Saraiva, Rafael Meliani Velloso - https://github.com/RafaelMV123/TRABALHO-DO-SIMOES
- Hoth: Henrique Freitas, Jayro Boy, Vinícius Finke - https://github.com/JayroBoy/OrgComp2018
- Snake Trifásico: Eduardo Zaboto Mirolli, Leonardo Bellini dos Reis, Vinícius Luiz da Silva Genésio - https://github.com/LeoBellini/jogo
- Bomberman: Gustavo Antonio Perin, João Vitor Monteiro, Marcelo Augusto Dos Reis - https://github.com/Bobagi/Bomberman
- Insira o seu projeto AQUI
- ICMC WARS: Gabriel Angelo Sgarbi Cocenza, Ingrid Hiromi Yagui Otani, Maria Vitória do N. Inocencio - https://github.com/gabrielcocenza/ICMC_WARS



# Apresentação da Disciplina

Professor: Eduardo do Valle Simões
Email: Simoes AT icmc.usp.br
Departamento de Sistemas de Computação – ICMC - USP
Grupo de Sistemas Embarcados e Evolutivos
Laboratório de Computação Reconfigurável

Organização de Computadores Digitais
Créditos Aula: 4
Créditos Trabalho: 1

### Objetivos
Introduzir conceitos de organização e arquitetura de computadores.

### Programa 
Arquitetura de von Neumann: conceitos, evolução, RISC e CISC, e processadores modernos. Implementação do Ciclo de Instrução: aspectos estruturais, funcionais e de desempenho do nível de microarquitetura. Unidade de controle hardwired e microprogramada. Noções de paralelismo no nível de microarquitetura. Subsistemas de memória: aspectos estruturais, funcionais e de desempenho. Entrada e Saída (E/S): organização, técnicas e sistemas de interconexão atuais. Arquiteturas paralelas: conceitos, taxonomia e exemplos.


### Avaliação
Serão realizados 1 prova e um tabalho sobre os assuntos do programa.
1ª Prova: Dia xx/xx/2022 

REC: xx/xx/2022

A nota final será calculada pela média ponderada dessas notas obtidas pelo aluno no decorrer do semestre.
Norma de Recuperação:
Para a aprovação pela recuperação (nota final < 5), deve-se usar este critério: (NP-2) / 5 * Mrec + 7 - NP, se Mrec >= 5; ou Max { NP, Mrec }, se Mrec < 5
 

### Bibliografia
- Livro Texto - PATTERSON, D. A., HENNESSY, J.L. Computer Organization and Design, Fifth Edition: The Hardware/Software Interface. Morgan Kaufmann Publishers, 2013. 
- Bibliografia Complementar 
  - MONTEIRO, M.A. Introdução à Organização de Computadores, 3a ed. Livros Técnicos e Científico Editora SA, 1996.
  - PATTERSON, D. A., HENNESSY, J.L. Computer Organization and Design RISC-V Edition: The Hardware Software Interface. Morgan Kaufmann Publishers, 2020.
  - PATTERSON, D. A., HENNESSY, J.L. Computer Organization and Design Arm Edition: The Hardware Software Interface. Morgan Kaufmann Publishers, 2016.
  - STALLINGS, W. Computer Organization and Architecture: designing for performance. 10th Edition, Pearson Education, 2015. 
  - TANENBAUM, A.S. Structured Computer Organization. 6th Edition. Pearson Education, 2012.

# Programa da Disciplina:
A seguir, vários tópicos da ementa da disciplina é apresentada com uma descrição da maneira com que cada tópico será abordado e dos capítulos dos Livros (PATTERSON - 3rd ed, 2003 e MONTEIRO - 5a ed, 2007) que contém cada tópico:

1) Revisão de conceitos sobre blocos lógicos/básicos; subsistemas de memória, organização, síntese e análise;
- duas aulas iniciais de revisão de todas as estruturas necessárias para a disciplina: multiplexadores, registradores, memórias (construção, barramentos, operação), ULA, state-machine, barramentos e interconexões.
- Será dada uma lista de exercícios para testar o conhecimento dos alunos nestes pontos, levantar deficiências e corrigi-las o mais cedo possível.
- As aulas seguintes, apresentam o projeto incremental de um processador contendo ULA, Máquina de Controle, Memória, registradores e dispositivos de I/O, servindo também de revisão pratica de implementação destes sub-sistemas e suas interconexões.
- PATTERSON Cap. 1.1 a 1.3, 3.1 a 3.5 (3rd ed, 2003)
- MONTEIRO Cap. 1.1.3, 3.3.1.2, 3.4.1, 4.2.1, 4.2.3, 4.3.3, 6.2.1, 6.2.2, 6.3, 6.4, 6.5., E.1 (5a ed, 2007)

2) Arquitetura de processadores, elementos básicos, operação geral, macro instruções e microinstruções, unidade de controle, fundamentos, desenvolvimento e implementação.
- Serão vistas as arquiteturas de Harvard e Von Neumann e será feita uma breve caracterização de maquinas RISC e CISC e seus elementos básicos.
- Um processador genérico (baseado na arquitetura Von Neumann / RISC) será implementado progressivamente e serão dados diversos exemplos de operação de suas subestruturas para todo um conjunto completo de instruções (ex.: ADD, LOAD, STORE, CALL, JUMP, PUSH, PULL, SHIFT ...)
- Serão apresentadas unidade de controle implementadas por elementos lógicos e por microprograma. Será também apresentado progressivamente todo o algoritmo de uma unidade de controle capaz de controlar as operações do conjunto de instruções do processador implementado.
- Serão apresentadas as características de sistemas de memória.
- Estes pontos serão vistos em uma seqüência de varias aulas, nas quais o processador e a máquina de controle serão construídos para executar o conjunto de instruções, uma a uma, revendo então seus fundamentos, desenvolvimento e implementação.
- PATTERSON Cap. 2.1 a 2.8, 5.1 a 5.5, 5.7, 7.1 a 7.2 (3rd ed, 2003)
- MONTEIRO Cap.8.1 a 8.5, 11.1, 11.2, 11.3.1, 11.3.2, 11.3.3 (5a ed, 2007)

3) Técnicas para organização de E/S, uso de DMA. Barramentos, conceitos gerais, estudos de casos.
- Barramentos, E/S e DMA serão vistos em duas fases:
a) Durante a revisão e a implementação progressiva do processador, varias noções praticas de diversos tipos de barramentos (controle, endereço, dados) e de dispositivos de E/S serão apresentadas. Inclusive, será construída em aula uma interface de leitura de teclado e uma controladora de vídeo (bastante simplificada: Texto 20 linhas x 16 colunas), ilustrando um mapa de caracteres e uma memória de vídeo. Serão também apresentados os conceitos de portos de entrada e saída em microcontroladores e interrupções de E/S.
b) Serão apresentados em transparências vários exemplos de hierarquia e controle (arbitragem) de barramentos, E/S Programada e os conceitos, operação e exemplos de DMA. As transparências serão utilizadas para apresentar (e não resumir) as informações conceituais do Livro sobre estes temas.
- PATTERSON Cap. 8.1 a 8.7 (3rd ed, 2003)
- MONTEIRO Cap. 10.1 a 10.3, 10.5, D.3.1, D.3.2 (5a ed, 2007)

4) Noções de Linguagens Montadoras.
- Serão apresentados exemplos de compilação de linguagens de alto nível para instruções Assembly e a utilização de montadores para converter linguagem montadora em linguagem de máquina, apresentando exemplos simples de transformação de linguagem C em linguagem montadora, como, por exemplo, inicialização de variáveis ("Int A = 5;"), Loops (For), e condicional (Se-Entao).
- Também serão vistos vários exemplos de desenvolvimento de programas em linguagem Assembly realizando diversas funcionalidades inclusive E/S.
- PATTERSON Cap. 2.5 a 2.8, 2.10, 2.13, 2.15 (3rd ed, 2003)
- MONTEIRO Cap. C.2, C.3, C.6 (5a ed, 2007)


# Metodologia:

- As aulas serão principalmente expositivas explicando-se na lousa os conteúdos do livro. Sendo assim, a cópia do material dado na lousa possibilitará a obtenção de uma síntese bem exemplificada do conteúdo do livro.
- Serão utilizadas transparências, apresentando os tópicos: arquitetura de processadores, elementos básicos, vários exemplos de hierarquia e controle (arbitragem) de barramentos, E/S Programada, configurações de memória e o conceito, operação e exemplos de DMA.
- Será disponibilizada uma ampla lista de exercícios sobre toda a matéria e marcada uma aula específica para tirar as duvidas dos alunos sobre os exercícios.
- Serão apresentadas em aula e disponibilizadas aos alunos ferramentas de CAD para a programação e simulação de processadores. Nestas ferramentas, serão apresentados a arquitetura do processador, seu mapa de memória, conjunto de instruções e programas em Assembly controlando dispositivos como sensores, motores e esteiras e alguns jogos.
- Será disponibilizada uma ferramenta que é um simulador de operação de um processador bastante semelhante ao que será apresentado e implementado em aula, desenvolvida por alunos veteranos do ICMC, incluindo os códigos (feitos em linguagem C) do algoritmo que simula os módulos do processador, o algoritmo da máquina de controle e um montador para os programas em linguagem de máquina. Com esta ferramenta, os alunos podem editar o processador, incluir novas instruções, reprogramar a máquina de controle para controlar estas novas instruções e editar o montador para montá-las para o código de maquina do processador.
- Serão também oferecidas aulas opcionais para a turma no laboratório, na qual serão apresentados a linguagem VHDL e a utilização das placas de FPGA recentemente adquiridas para a implementação de processadores próprios dos alunos. Serão fornecidas transparências com a síntese desta metodologia para os alunos.
- Alguns alunos serão convidados a apresentar implementações para seus colegas utilizando as ferramentas apresentadas para implementar software em linguagem montadora e controlar dispositivos com os seus microcontroladores, bem como os que implementarem processadores próprios em software com o simulador ou em hardware na placa de FPGA.
- Todo este material será disponibilizado diretamente aos alunos durante as aulas e através do gitlab.


